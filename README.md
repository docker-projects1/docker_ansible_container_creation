## Docker - container creation - Ansible

## Versions

- Ansible: 2.9.6
- Docker client: 20.10.1-ce

## Yaml file

```
---
- hosts: all
  become: true
  vars:
    containername: "dockercontainer"
    image: "ubuntu"
  tasks:
    - name: Dependencies Installation
      apt: 
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - software-properties-common
          - python3-pip
          - virtualenv
          - python3-setuptools
    
    - name: key download
      apt_key:
        url:  https://download.docker.com/linux/ubuntu/gpg
        state: present
    - name: repositry
      apt_repository:
        repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable
        state: present

    - name: Docker Engine Installation
      apt:
        name: docker-ce
        state: present

    - name: Install Docker module
      pip:
        name: docker
    - name: Container Creation
      docker_container:
        name: "{{ containername }}"
        image: "{{ image }}"
        state: present




```


